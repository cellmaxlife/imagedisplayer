//*******************************************************************************
// MFC Scrolling demo
// Code is provided as is. You may do whatever you like with it.
//*******************************************************************************
#include "stdafx.h"
#include "ImageDisplayer.h"

#define MIN_ZOOM_FACTOR1	4.0
#define MAX_ZOOM_FACTOR1	32.0
#define MIN_ZOOM_FACTOR2    33.0
#define MAX_ZOOM_FACTOR2	321.0

#define MAX_SCREEN_WIDTH	2400
#define MAX_SCREEN_HEIGHT	1600

// global constant
const int LINESIZE = 8;
// our application object. must be global
ImageDisplayerApp theApp;
// The usual initialisation
BOOL ImageDisplayerApp::InitInstance()
{
	CFileDialog dlg(TRUE,    // open
		NULL,    // no default extension
		NULL,    // no initial file name
		OFN_FILEMUSTEXIST
		| OFN_HIDEREADONLY,
		_T("Red Channel TIFF files (*.tif)|*.tif"), NULL, 0, TRUE);
	if (dlg.DoModal() == IDOK)
	{
		CString filename = dlg.GetPathName();
		m_pMainWnd = new ImageDisplayerWnd(filename);
		m_pMainWnd->ShowWindow(m_nCmdShow);
		m_pMainWnd->UpdateWindow();
		return TRUE;
	}
	else
	{
		AfxMessageBox(_T("Failed to specify a Red Channel TIFF file"));
		return FALSE;
	}
}
// The message map
BEGIN_MESSAGE_MAP(ImageDisplayerWnd, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_WM_MOUSEWHEEL()
	ON_WM_CLOSE()
END_MESSAGE_MAP()
// constructor to create the window
ImageDisplayerWnd::ImageDisplayerWnd(CString filename)
{
	m_Filename = filename;
	Create(NULL, _T("ImageDisplayer(V1)"), WS_OVERLAPPEDWINDOW | WS_HSCROLL | WS_VSCROLL);
	Leica_Red_Prefix = _T("");
	Leica_Green_Prefix = _T("");
	Leica_Blue_Prefix = _T("");
	Zeiss_Red_Postfix = _T("");
	Zeiss_Green_Postfix = _T("");
	Zeiss_Blue_Postfix = _T("");
}

void ImageDisplayerWnd::OnClose()
{
	delete m_RedTIFFData;
	delete m_GreenTIFFData;
	delete m_BlueTIFFData;
	CFrameWnd::OnClose();
}

// this gets called on a WM_CREATE
int ImageDisplayerWnd::OnCreate(LPCREATESTRUCT createstruct)
{
	// always call the base class OnCreate to give the FrameWnd
	// chance to initialise
	if (CFrameWnd::OnCreate(createstruct) == -1) return -1;
	CString version;
	version.Format(_T("LeicaParamsV3.txt"));
	m_CTCParams.LoadCTCParams(version);
	LoadDefaultSettings();
	m_RedTIFFData = new CSingleChannelTIFFData();
	m_GreenTIFFData = new CSingleChannelTIFFData();
	m_BlueTIFFData = new CSingleChannelTIFFData();
	m_ZoomFactor = MIN_ZOOM_FACTOR1;
	if (!LoadTIFFImages(m_Filename))
	{
		AfxMessageBox(_T("Failed to load TIFF Image File"));
		return TRUE;
	}
	// get a client area DC for this window
	CClientDC dc(this);
	m_dcMem.CreateCompatibleDC(&dc);
	CImage image;
	image.Create(MAX_SCREEN_WIDTH, -MAX_SCREEN_HEIGHT, 24);
	BYTE *pCursor = (BYTE *)image.GetBits();
	memset(pCursor, 0, sizeof(BYTE) * image.GetPitch() * MAX_SCREEN_HEIGHT);
	CBitmap bitmap;
	bitmap.Attach(image.Detach());
	CBitmap *pOldMap = m_dcMem.SelectObject(&bitmap);
	if (pOldMap != NULL)
		pOldMap->DeleteObject();
	m_ZoomFactor = MIN_ZOOM_FACTOR1;
	m_DisplayMap1 = true;
	ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
	ViewHeight_ = (int)(m_ImageHeight1* m_ZoomFactor);
	HScrollPos_ = 0;
	VScrollPos_ = 0;
	// return 0 if all went well
	return 0;
}
// this gets called on WM_SIZE
void ImageDisplayerWnd::OnSize(UINT type, int x, int y)
{
	// call base class OnSize
	CFrameWnd::OnSize(type, x, y);
	HPageSize_ = x;
	VPageSize_ = y;
	SCROLLINFO si;
	si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewWidth_;
	si.nPos = HScrollPos_;
	si.nPage = HPageSize_;
	SetScrollInfo(SB_HORZ, &si, TRUE);
	si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewHeight_;
	si.nPos = VScrollPos_;
	si.nPage = VPageSize_;
	SetScrollInfo(SB_VERT, &si, TRUE);
}
// This gets called on a WM_PAINT
void ImageDisplayerWnd::OnPaint()
{
	// get a painting DC
	CPaintDC dc(this);
	CRect rect;
	GetClientRect(&rect);
	if (m_DisplayMap1)
	{
		dc.SetMapMode(MM_ISOTROPIC);
		dc.SetWindowExt(100, 100);
		dc.SetViewportExt((int) (100 * m_ZoomFactor), (int) (100 * m_ZoomFactor));
		int startX = (int)(HScrollPos_ / m_ZoomFactor);
		int startY = (int)(VScrollPos_ / m_ZoomFactor);
		int width = rect.Width();
		if ((m_ImageWidth1 - startX) < width)
			width = m_ImageWidth1 - startX;
		else if (width > MAX_SCREEN_WIDTH)
			width = MAX_SCREEN_WIDTH; 
		int height = rect.Height();
		if ((m_ImageHeight1 - startY) < height)
			height = m_ImageHeight1 - startY;
		else if (height > MAX_SCREEN_HEIGHT)
			height = MAX_SCREEN_HEIGHT;
		m_Image1.BitBlt(m_dcMem, 0, 0, width, height, startX, startY, SRCCOPY);
		dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &m_dcMem, 0, 0, SRCCOPY);
	}
	else 
	{
		dc.SetMapMode(MM_ISOTROPIC);
		dc.SetWindowExt(100, 100);
		dc.SetViewportExt((int)(100 * (m_ZoomFactor - MAX_ZOOM_FACTOR1)), (int)(100 * (m_ZoomFactor - MAX_ZOOM_FACTOR1)));
		int startX = (int)(HScrollPos_ / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		int startY = (int)(VScrollPos_ / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		int width = rect.Width();
		if ((m_ImageWidth2 - startX) < width)
			width = m_ImageWidth2 - startX;
		else if (width > MAX_SCREEN_WIDTH)
			width = MAX_SCREEN_WIDTH;
		int height = (int)(rect.Height() / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		if ((m_ImageHeight2 - startY) < height)
			height = m_ImageHeight2 - startY;
		else if (height > MAX_SCREEN_HEIGHT)
			height = MAX_SCREEN_HEIGHT;
		m_Image2.BitBlt(m_dcMem, 0, 0, width, height, startX, startY, SRCCOPY);
		dc.BitBlt(rect.left, rect.top, rect.Width(), rect.Height(), &m_dcMem, 0, 0, SRCCOPY);
	}
}

// this is called on WM_HSCROLL
void ImageDisplayerWnd::OnHScroll(UINT code, UINT pos, CScrollBar* sb)
{
	// number of pixels to be scrolled
	int delta;
	// which sb msg was sent. Set delta accordingly
	switch (code)
	{
	case SB_LINELEFT:
		delta = -LINESIZE;
		break;
	case SB_PAGELEFT:
		delta = -HPageSize_;
		break;
	case SB_THUMBTRACK:
		delta = static_cast<int>(pos)-HScrollPos_;
		break;
	case SB_PAGERIGHT:
		delta = HPageSize_;
		break;
	case SB_LINERIGHT:
		delta = LINESIZE;
		break;
	default:
		// ignore other messages
		return;
	}
	int scrollpos = HScrollPos_ + delta;
	int maxpos = ViewWidth_ - HPageSize_;
	if (scrollpos < 0)
		delta = -HScrollPos_;
	else if (scrollpos >= maxpos)
		delta = maxpos - HScrollPos_;
	// if we need to scroll do the scrolling
	if (delta != 0)
	{
		HScrollPos_ += delta;
		SetScrollPos(SB_HORZ, HScrollPos_, TRUE);
		ScrollWindow(-delta, 0);
	}
}
// This is called on WM_VSCROLL
void ImageDisplayerWnd::OnVScroll(UINT code, UINT pos, CScrollBar* sb)
{
	// number of pixels to be scrolled
	int delta;
	// which sb msg was it. set delta accordingly
	switch (code)
	{
	case SB_LINEUP:
		delta = -LINESIZE;
		break;
	case SB_PAGEUP:
		delta = -VPageSize_;
		break;
	case SB_THUMBTRACK:
		delta = static_cast<int>(pos)-VScrollPos_;
		break;
	case SB_PAGEDOWN:
		delta = VPageSize_;
		break;
	case SB_LINEDOWN:
		delta = LINESIZE;
		break;
	default:
		return;
	}
	int scrollpos = VScrollPos_ + delta;
	int maxpos = ViewHeight_ - VPageSize_;
	if (scrollpos < 0)
		delta = -VScrollPos_;
	else if (scrollpos >= maxpos)
		delta = maxpos - VScrollPos_;
	// if we need to scroll then do the scrolling
	if (delta != 0)
	{
		VScrollPos_ += delta;
		SetScrollPos(SB_VERT, VScrollPos_, TRUE);
		ScrollWindow(0, -delta);
	}
}
// this is called on WM_MOUSEWHEEL
BOOL ImageDisplayerWnd::OnMouseWheel(UINT flags, short zdelta, CPoint point)
{
	CRect rect(0, 0, 0, 0);
	GetClientRect(&rect);
	double currentXCenterPos = 0.0;
	double currentYCenterPos = 0.0;
	if (m_DisplayMap1)
	{
		currentXCenterPos = HScrollPos_ * m_ZoomFactor;
		currentYCenterPos = VScrollPos_ * m_ZoomFactor;
	}
	else
	{
		currentXCenterPos = HScrollPos_ * (m_ZoomFactor - MAX_ZOOM_FACTOR1);
		currentYCenterPos = VScrollPos_ * (m_ZoomFactor - MAX_ZOOM_FACTOR1);
	}

	m_ZoomFactor += ((double) zdelta / 300.0);
	if (m_ZoomFactor < MIN_ZOOM_FACTOR1)
		m_ZoomFactor = MIN_ZOOM_FACTOR1;
	else if ((m_ZoomFactor > MAX_ZOOM_FACTOR1) && (m_ZoomFactor < MIN_ZOOM_FACTOR2))
		m_ZoomFactor = MIN_ZOOM_FACTOR2;
	else if (m_ZoomFactor > MAX_ZOOM_FACTOR2)
		m_ZoomFactor = MAX_ZOOM_FACTOR2;
	SCROLLINFO si;
	if (m_ZoomFactor <= MAX_ZOOM_FACTOR1)
	{
		m_DisplayMap1 = true;
		ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
		ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
		HPageSize_ = rect.Width();
		VPageSize_ = rect.Height();
		HScrollPos_ = (int)(currentXCenterPos / m_ZoomFactor);
		VScrollPos_ = (int)(currentYCenterPos / m_ZoomFactor);
	}
	else
	{
		m_DisplayMap1 = false;
		ViewWidth_ = (int)(m_ImageWidth2 * (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		ViewHeight_ = (int)(m_ImageHeight2 * (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		HPageSize_ = rect.Width();
		VPageSize_ = rect.Height();
		HScrollPos_ = (int)(currentXCenterPos / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
		VScrollPos_ = (int)(currentYCenterPos / (m_ZoomFactor - MAX_ZOOM_FACTOR1));
	}
	
	si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewWidth_;
	si.nPos = HScrollPos_;
	si.nPage = HPageSize_;
	SetScrollInfo(SB_HORZ, &si, TRUE);
	si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
	si.nMin = 0;
	si.nMax = ViewHeight_;
	si.nPos = VScrollPos_;
	si.nPage = VPageSize_;
	SetScrollInfo(SB_VERT, &si, TRUE);
	Invalidate();
	return TRUE;
}

void ImageDisplayerWnd::LoadDefaultSettings()
{
	CStdioFile theFile;
	if (theFile.Open(_T(".\\DefaultSettings.txt"), CFile::modeRead | CFile::typeText))
	{
		CString textline;
		while (theFile.ReadString(textline))
		{
			if (textline.Find(_T("Leica_Red_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Red_Prefix=");
				Leica_Red_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Green_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Green_Prefix=");
				Leica_Green_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Leica_Blue_Prefix=")) > -1)
			{
				CString tag = _T("Leica_Blue_Prefix=");
				Leica_Blue_Prefix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Red_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Red_Postfix=");
				Zeiss_Red_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Green_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Green_Postfix=");
				Zeiss_Green_Postfix = textline.Mid(tag.GetLength());
			}
			else if (textline.Find(_T("Zeiss_Blue_Postfix=")) > -1)
			{
				CString tag = _T("Zeiss_Blue_Postfix=");
				Zeiss_Blue_Postfix = textline.Mid(tag.GetLength());
			}
		}
		theFile.Close();
	}
}

bool ImageDisplayerWnd::LoadTIFFImages(CString filename)
{
	bool ret = false;
	CString message;
	WCHAR *ch1 = _T("\\");
	int index = filename.ReverseFind(*ch1);
	if (index == -1)
		return ret;
	CString filename1 = filename.Mid(index+1);

	index = filename1.Find(Leica_Red_Prefix, 0);
	if (index == -1)
		index = filename1.Find(Zeiss_Red_Postfix, 0);
	if (index == -1)
	{
		message.Format(_T("Image Filename %s doesn't have either Leica Image Prefix %s or Zeiss Image Postfix %s"), filename1, Leica_Red_Prefix, Zeiss_Red_Postfix);
		AfxMessageBox(message);
		return ret;
	}
	ret = m_RedTIFFData->LoadRawTIFFFile(filename);
	if (!ret)
	{
		message.Format(_T("Failed to load %s"), filename);
		AfxMessageBox(message);
		return ret;
	}
	else
	{
		index = filename.Find(Leica_Red_Prefix, 0);
		if (index == -1)
		{
			index = filename.Find(Zeiss_Red_Postfix);
			CString samplePathName = filename.Mid(0, index);
			CString filename2 = samplePathName + Zeiss_Green_Postfix;
			ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
			if (ret)
			{
				filename2 = samplePathName + Zeiss_Blue_Postfix;
				ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
				if (ret)
				{
					message.Format(_T("ZeissParamsV3.txt"));
					m_CTCParams.LoadCTCParams(message);
				}
				else
				{
					message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
					AfxMessageBox(message);
					return ret;
				}
			}
			else
			{
				message.Format(_T("Failed to load Green TIFF File %s"), filename2);
				AfxMessageBox(message);
				return ret;
			}
		}
		else
		{
			CString postfix = filename1.Mid(Leica_Red_Prefix.GetLength());
			index = filename.Find(Leica_Red_Prefix, 0);
			CString pathname = filename.Mid(0, index);
			CString filename2;
			filename2.Format(_T("%s%s%s"), pathname, Leica_Green_Prefix, postfix);
			ret = m_GreenTIFFData->LoadRawTIFFFile(filename2);
			if (!ret)
			{
				message.Format(_T("Failed to load Green TIFF File %s"), filename2);
				AfxMessageBox(message);
				return ret;
			}
			else
			{
				filename2.Format(_T("%s%s%s"), pathname, Leica_Blue_Prefix, postfix);
				ret = m_BlueTIFFData->LoadRawTIFFFile(filename2);
				if (!ret)
				{
					message.Format(_T("Failed to load Blue TIFF File %s"), filename2);
					AfxMessageBox(message);
					return ret;
				}
				else
				{
					message.Format(_T("LeicaParamsV3.txt"));
					m_CTCParams.LoadCTCParams(message);
				}
			}
		}
		filename1 = _T("ImageDisplayer - ") + filename1;
		SetWindowText(filename1);
	}
	CopyToRGBImage();
	return true;
}

void ImageDisplayerWnd::CopyToRGBImage()
{
	m_ImageWidth1 = m_RedTIFFData->GetSubsampledWidth();
	m_ImageHeight1 = m_RedTIFFData->GetSubsampledHeight();
	int width = m_ImageWidth1;
	int height = m_ImageHeight1;
	unsigned short *redImg = new unsigned short[width * height];
	unsigned short *greenImg = new unsigned short[width * height];
	unsigned short *blueImg = new unsigned short[width * height];
	int redCutoff = m_RedTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_RED_CUTOFF];
	int greenCutoff = m_GreenTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CUTOFF];
	int blueCutoff = m_BlueTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CUTOFF];
	int redContrast = m_RedTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_RED_CONTRAST];
	int greenContrast = m_GreenTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_GREEN_CONTRAST];
	int blueContrast = m_BlueTIFFData->GetCPI() + m_CTCParams.m_CTCParams[(int)PARAM_BLUE_CONTRAST];

	m_Image1.Create(m_ImageWidth1, -m_ImageHeight1, 24);
	BYTE *ptr = (BYTE *)m_Image1.GetBits();
	int stride = m_Image1.GetPitch() - 3 * width;
	bool redRet = m_RedTIFFData->GetSubsampleImage(redImg);
	bool greenRet = m_GreenTIFFData->GetSubsampleImage(greenImg);
	bool blueRet = m_BlueTIFFData->GetSubsampleImage(blueImg);
	if (redRet && greenRet && blueRet)
	{
		unsigned short *ptrRed = redImg;
		unsigned short *ptrGreen = greenImg;
		unsigned short *ptrBlue = blueImg;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++, ptrRed++, ptrGreen++, ptrBlue++)
			{
				BYTE redByte = GetContrastEnhancedByte(*ptrRed, redContrast, redCutoff);
				BYTE greenByte = GetContrastEnhancedByte(*ptrGreen, greenContrast, greenCutoff);
				BYTE blueByte = GetContrastEnhancedByte(*ptrBlue, blueContrast, blueCutoff);
				*ptr++ = blueByte;
				*ptr++ = greenByte;
				*ptr++ = redByte;
			}
			if (stride > 0)
				ptr += stride;
		}
	}
	delete[] redImg;
	delete[] greenImg;
	delete[] blueImg;
	
	m_ImageWidth2 = m_RedTIFFData->GetImageWidth();
	m_ImageHeight2 = m_RedTIFFData->GetImageHeight();
	width = m_ImageWidth2;
	height = m_ImageHeight2;
	redImg = new unsigned short[width];
	greenImg = new unsigned short[width];
	blueImg = new unsigned short[width];

	m_Image2.Create(m_ImageWidth2, -m_ImageHeight2, 24);
	ptr = (BYTE *)m_Image2.GetBits();
	stride = m_Image2.GetPitch() - 3 * width;
	for (int i = 0; i < height; i++)
	{
		bool redRet = m_RedTIFFData->GetImageRegion(0, i, width, 1, redImg);
		bool greenRet = m_GreenTIFFData->GetImageRegion(0, i, width, 1, greenImg);
		bool blueRet = m_BlueTIFFData->GetImageRegion(0, i, width, 1, blueImg);
		if (redRet && greenRet && blueRet)
		{
			unsigned short *ptrRed = redImg;
			unsigned short *ptrGreen = greenImg;
			unsigned short *ptrBlue = blueImg;
			for (int j = 0; j < width; j++, ptrRed++, ptrGreen++, ptrBlue++)
			{
				BYTE redByte = GetContrastEnhancedByte(*ptrRed, redContrast, redCutoff);
				BYTE greenByte = GetContrastEnhancedByte(*ptrGreen, greenContrast, greenCutoff);
				BYTE blueByte = GetContrastEnhancedByte(*ptrBlue, blueContrast, blueCutoff);
				*ptr++ = blueByte;
				*ptr++ = greenByte;
				*ptr++ = redByte;
			}
			if (stride > 0)
				ptr += stride;
		}
	}
	delete[] redImg;
	delete[] greenImg;
	delete[] blueImg;
}

BYTE ImageDisplayerWnd::GetContrastEnhancedByte(unsigned short value, int contrast, int cutoff)
{
	BYTE result = 0;
	int value1 = value - cutoff;
	int maxValue = contrast;
	maxValue -= cutoff;
	if (maxValue < 255)
		maxValue = 255;

	if (value1 < 0)
		result = (BYTE)0;
	else if (value1 > maxValue)
		result = (BYTE)255;
	else
	{
		result = (BYTE)(255.0 * value1 / maxValue);;
	}
	return result;
}

BOOL ImageDisplayerWnd::PreTranslateMessage(MSG* pMsg)
{
	if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_RETURN))
	{
		return TRUE; // this doesn't need processing anymore
	}
	else if ((pMsg->message == WM_KEYDOWN) &&
		(pMsg->wParam == VK_ESCAPE))
	{
		CRect rect(0, 0, 0, 0);
		GetClientRect(&rect);
		
		m_ZoomFactor = MIN_ZOOM_FACTOR1;
		ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
		ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
		m_DisplayMap1 = true;
		ViewWidth_ = (int)(m_ImageWidth1 * m_ZoomFactor);
		ViewHeight_ = (int)(m_ImageHeight1 * m_ZoomFactor);
		HPageSize_ = rect.Width();
		VPageSize_ = rect.Height();
		HScrollPos_ = 0;
		VScrollPos_ = 0;
		SCROLLINFO si;
		si.fMask = SIF_PAGE | SIF_RANGE | SIF_POS;
		si.nMin = 0;
		si.nMax = ViewWidth_;
		si.nPos = HScrollPos_;
		si.nPage = HPageSize_;
		SetScrollInfo(SB_HORZ, &si, TRUE);
		si.fMask = SIF_RANGE | SIF_PAGE | SIF_POS;
		si.nMin = 0;
		si.nMax = ViewHeight_;
		si.nPos = VScrollPos_;
		si.nPage = VPageSize_;
		SetScrollInfo(SB_VERT, &si, TRUE);
		Invalidate();
		return TRUE;
	}
	return CFrameWnd::PreTranslateMessage(pMsg);
}