#pragma once

#include "afxwin.h"
#define SUBSAMPLERATE	32

class CSingleChannelTIFFData
{
public:
	CSingleChannelTIFFData();
	virtual ~CSingleChannelTIFFData();

protected:
	int m_Width;
	int m_Height;
	unsigned short **m_Image;
	bool m_ZeissData;
	void FreeImage();
	unsigned short m_CPI;

public:
	bool LoadRawTIFFFile(CString filename);
	int GetImageWidth(void);
	int GetImageHeight(void);
	bool GetImageRegion(int x0, int y0, int width, int height, unsigned short *image);
	bool IsZeissData();
	unsigned short GetCPI();
	int GetSubsampledWidth();
	int GetSubsampledHeight();
	bool GetSubsampleImage(unsigned short *image);
	void CleanUpExclusionArea(int centerX, int centerY, int radiusX, int radiusY);
};


