
#pragma once
#include "CTCParams.h"
#include "SingleChannelTIFFData.h"
#include <afxwin.h>

class ImageDisplayerApp : public CWinApp
{
public:
	// override InitInstance to do our initialisation
	virtual BOOL InitInstance();
};

class ImageDisplayerWnd : public CFrameWnd
{
public:
	// constructor to create the window
	ImageDisplayerWnd(CString filename);
private:
	int ViewWidth_;
	int ViewHeight_;
	int HScrollPos_;
	int VScrollPos_;
	int HPageSize_;
	int VPageSize_;
	CString m_Filename;
protected:
	BYTE GetContrastEnhancedByte(unsigned short value, int contrast, int cutoff);
	// declare our handlers
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT);
	afx_msg void OnSize(UINT, int, int);
	afx_msg void OnHScroll(UINT, UINT, CScrollBar*);
	afx_msg void OnVScroll(UINT, UINT, CScrollBar*);
	afx_msg BOOL OnMouseWheel(UINT, short, CPoint);
	afx_msg void OnClose();
	// this declares the message map and should be
	// the last class member
	DECLARE_MESSAGE_MAP()
	CCTCParams m_CTCParams;
	CSingleChannelTIFFData *m_RedTIFFData;
	CSingleChannelTIFFData *m_GreenTIFFData;
	CSingleChannelTIFFData *m_BlueTIFFData;
	CString Leica_Red_Prefix;
	CString Leica_Green_Prefix;
	CString Leica_Blue_Prefix;
	CString Zeiss_Red_Postfix;
	CString Zeiss_Green_Postfix;
	CString Zeiss_Blue_Postfix;
	void LoadDefaultSettings();
	void CopyToRGBImage();
	bool LoadTIFFImages(CString filename);
	int m_ImageWidth1, m_ImageHeight1;
	int m_ImageWidth2, m_ImageHeight2;
	CDC m_dcMem;
	CImage m_Image1;
	CImage m_Image2;
	bool m_DisplayMap1;
	double m_ZoomFactor;
	BOOL PreTranslateMessage(MSG* pMsg);
};

