#include "stdafx.h"
#include "SingleChannelTIFFData.h"

#define CPI_RANGE_START 200
#define CPI_RANGE_END	400
#define CPI_RANGE_LENGTH 200

#define NUM_TAGS_IN_LEICA_TIFF 13
#define NUM_TAGS_IN_ZEISS_TIFF 21

const int TIFFTAG_IMAGEWIDTH = 256;	/* image width in pixels */
const int TIFFTAG_IMAGELENGTH = 257;	/* image height in pixels */
const int TIFFTAG_BITSPERSAMPLE = 258;	/* bits per channel (sample) */
const int TIFFTAG_COMPRESSION = 259;	/* data compression technique */
const int COMPRESSION_NONE = 1;	/* dump mode */
const int COMPRESSION_CCITTRLE = 2;	/* CCITT modified Huffman RLE */
const int COMPRESSION_CCITTFAX3 = 3;	/* CCITT Group 3 fax encoding */
const int COMPRESSION_CCITTFAX4 = 4;	/* CCITT Group 4 fax encoding */
const int COMPRESSION_LZW = 5;	/* Lempel-Ziv  & Welch */
const int COMPRESSION_JPEG = 6;	/* !JPEG compression */
const int TIFFTAG_PHOTOMETRIC = 262;	/* photometric interpretation */
const int PHOTOMETRIC_MINISWHITE = 0;	/* min value is white */
const int PHOTOMETRIC_MINISBLACK = 1;	/* min value is black */
const int PHOTOMETRIC_RGB = 2;	/* RGB color model */
const int PHOTOMETRIC_PALETTE = 3;	/* color map indexed */
const int PHOTOMETRIC_MASK = 4;	/* $holdout mask */
const int PHOTOMETRIC_SEPARATED = 5;	/* !color separations */
const int PHOTOMETRIC_YCBCR = 6;	/* !CCIR 601 */
const int PHOTOMETRIC_CIELAB = 8;	/* !1976 CIE L*a*b* */
const int TIFFTAG_STRIPOFFSETS = 273;	/* offsets to data strips */
const int TIFFTAG_SAMPLESPERPIXEL = 277;	/* samples per pixel */
const int TIFFTAG_ROWSPERSTRIP = 278;	/* rows per strip of data */
const int TIFFTAG_STRIPBYTECOUNTS = 279;	/* bytes counts for strips */

CSingleChannelTIFFData::CSingleChannelTIFFData()
{
	m_Width = 0;
	m_Height = 0;
	m_Image = NULL;
	m_CPI = 0;
}

CSingleChannelTIFFData::~CSingleChannelTIFFData(void)
{
	FreeImage();
}

void CSingleChannelTIFFData::FreeImage()
{
	if (m_Image != NULL)
	{
		for (int i = 0; i < m_Height; i++)
		{
			delete m_Image[i];
			m_Image[i] = NULL;
		}

		delete m_Image;
		m_Image = NULL;
	}
}

bool CSingleChannelTIFFData::LoadRawTIFFFile(CString filename)
{
	bool status = false;
	m_ZeissData = false;
	CFile theFile;
	bool theFileOpened = false;
	BYTE *buf = NULL;
	UINT32 *StripOffsetArray = NULL;
	UINT32 *StripByteCountArray = NULL;
	UINT32 *hist = NULL;

	try
	{
		FreeImage();
		if (theFile.Open(filename, CFile::modeRead))
		{
			theFileOpened = true;
			while (true)
			{
				ULONGLONG numBytes = theFile.GetLength();
				if (numBytes < 8)
				{
					break;
				}
				buf = new BYTE[8];
				UINT count = theFile.Read(buf, 8);
				if (count != 8)
				{
					break;
				}
				if ((buf[0] != buf[1]) || (buf[0] != 'I'))
				{
					break;
				}
				UINT32 *ptr;
				ptr = (UINT32*)&buf[4];
				if (*ptr >= numBytes)
				{
					break;
				}
				theFile.Seek(*ptr, CFile::begin);
				int len = (int)(numBytes - *ptr);
				if (buf != NULL)
				{
					delete[] buf;
					buf = NULL;
				}
				buf = new BYTE[len];
				count = theFile.Read(buf, len);
				if (count != len)
				{
					break;
				}
				UINT16 numTags = *((UINT16*)&buf[0]);
				if (numTags >= NUM_TAGS_IN_ZEISS_TIFF)
				{
					m_ZeissData = true;
				}

				UINT32 offset = 2;
				UINT16 tag;
				BOOL exitWhileLoop = FALSE;
				UINT32 CountForStripOffsetArray = 0;
				UINT32 OffsetToStripOffsetArray = 0;
				UINT32 CountForStripByteCountArray = 0;
				UINT32 OffsetToStripByteCountArray = 0;
				for (int i = 0; i < numTags; i++, offset += 12)
				{
					tag = *((UINT16 *)&buf[offset]);
					if (tag == TIFFTAG_IMAGEWIDTH)
					{
						m_Width = *((UINT32 *)&buf[offset + 8]);
					}
					else if (tag == TIFFTAG_IMAGELENGTH)
					{
						m_Height = *((UINT32 *)&buf[offset + 8]);
					}
					else if (tag == TIFFTAG_BITSPERSAMPLE)
					{
						int BitsPerPixel = *((UINT32 *)&buf[offset + 8]);
						if (BitsPerPixel != 16)
						{
							exitWhileLoop = TRUE;
							break;
						}
					}
					else if (tag == TIFFTAG_STRIPOFFSETS)
					{
						CountForStripOffsetArray = *((UINT32 *)&buf[offset + 4]);
						OffsetToStripOffsetArray = *((UINT32 *)&buf[offset + 8]);
					}
					else if (tag == TIFFTAG_STRIPBYTECOUNTS)
					{
						CountForStripByteCountArray = *((UINT32 *)&buf[offset + 4]);
						OffsetToStripByteCountArray = *((UINT32 *)&buf[offset + 8]);
					}
				}

				if (exitWhileLoop)
					break;

				if (CountForStripOffsetArray > 0)
					StripOffsetArray = new UINT32[CountForStripOffsetArray];
				else
					break;

				theFile.Seek(OffsetToStripOffsetArray, CFile::begin);
				len = (int)(4 * CountForStripOffsetArray);
				if (buf != NULL)
				{
					delete[] buf;
					buf = NULL;
				}
				buf = new BYTE[len];
				count = theFile.Read(buf, len);
				if (count != len)
				{
					break;
				}

				offset = 0;
				for (int j = 0; j < (int)CountForStripOffsetArray; j++, offset += 4)
				{
					StripOffsetArray[j] = *((UINT32 *)&buf[offset]);
				}

				if (CountForStripByteCountArray > 0)
					StripByteCountArray = new UINT32[CountForStripByteCountArray];
				else
					break;

				theFile.Seek(OffsetToStripByteCountArray, CFile::begin);
				len = (int)(4 * CountForStripByteCountArray);
				if (buf != NULL)
				{
					delete[] buf;
					buf = NULL;
				}
				buf = new BYTE[len];
				count = theFile.Read(buf, len);
				if (count != len)
				{
					break;
				}

				offset = 0;
				for (int j = 0; j < (int)CountForStripByteCountArray; j++, offset += 4)
				{
					StripByteCountArray[j] = *((UINT32 *)&buf[offset]);
				}

				if ((m_Width == 0) || (m_Height == 0))
					break;

				m_Image = new unsigned short*[m_Height];
				for (int i = 0; i < m_Height; i++)
					m_Image[i] = new unsigned short[m_Width];

				hist = new UINT32[CPI_RANGE_LENGTH];
				memset(hist, 0, sizeof(UINT32) * CPI_RANGE_LENGTH);

				int rowIndex = 0;
				int columnIndex = 0;
				int numPixels;
				for (int i = 0; i < (int)CountForStripOffsetArray; i++)
				{
					theFile.Seek(StripOffsetArray[i], CFile::begin);
					len = (int)StripByteCountArray[i];
					if (buf != NULL)
					{
						delete[] buf;
						buf = NULL;
					}
					buf = new BYTE[len];
					count = theFile.Read(buf, len);
					if (count != len)
					{
						exitWhileLoop = TRUE;
						break;
					}

					offset = 0;
					numPixels = (int)(len / 2);
					for (int j = 0; j < numPixels; j++, offset += 2)
					{
						unsigned short value = *((unsigned short *)&buf[offset]);
						if (m_ZeissData)
							value /= 4;
						if ((value >= CPI_RANGE_START) && (value < CPI_RANGE_END))
							hist[value-CPI_RANGE_START]++;
						m_Image[rowIndex][columnIndex] = value;
						columnIndex++;
						if (columnIndex == m_Width)
						{
							columnIndex = 0;
							rowIndex++;
						}
					}
				}

				theFile.Close();
				theFileOpened = false;

				UINT32 maxHistCount = 0;
				int maxHistIndex = 0;
				for (int i = 0; i < CPI_RANGE_LENGTH; i++)
				{
					if (hist[i] > maxHistCount)
					{
						maxHistCount = hist[i];
						maxHistIndex = i;
					}
				}

				delete[] hist;
				hist = NULL;

				m_CPI = maxHistIndex + CPI_RANGE_START;

				if (buf != NULL)
				{
					delete[] buf;
					buf = NULL;
				}

				if (StripByteCountArray != NULL)
				{
					delete StripByteCountArray;
					StripByteCountArray = NULL;
				}

				if (StripOffsetArray != NULL)
				{
					delete StripOffsetArray;
					StripOffsetArray = NULL;
				}

				if (exitWhileLoop)
					break;

				status = true;
				break;
			}
		}
	}
	catch (...)
	{
		if (theFileOpened)
		{
			theFile.Close();
			theFileOpened = false;
		}
		if (buf != NULL)
		{
			delete[] buf;
			buf = NULL;
		}

		if (StripByteCountArray != NULL)
		{
			delete StripByteCountArray;
			StripByteCountArray = NULL;
		}

		if (StripOffsetArray != NULL)
		{
			delete StripOffsetArray;
			StripOffsetArray = NULL;
		}

		if (hist != NULL)
		{
			delete[] hist;
			hist = NULL;
		}
	}
	return status;
}

int CSingleChannelTIFFData::GetImageWidth(void)
{
	return m_Width;
}

int CSingleChannelTIFFData::GetImageHeight(void)
{
	return m_Height;
}

bool CSingleChannelTIFFData::GetImageRegion(int x0, int y0, int width, int height, unsigned short *image)
{
	bool ret = false;
	CString message;
	
	while (TRUE)
	{
		if ((x0 < 0) || (y0 < 0) || ((x0 + width) > m_Width) || ((y0 + height) > m_Height))
		{
			break;
		}

		if (image == NULL)
		{
			break;
		}

		if (m_Image == NULL)
		{
			break;
		}

		for (int i = 0; i < height; i++)
		{
			memcpy(&image[width * i], &(m_Image[y0+i][x0]), sizeof(unsigned short) * width);
		}
		ret = true;
		break;
	}
	return ret;
}

bool CSingleChannelTIFFData::IsZeissData()
{
	return m_ZeissData;
}

unsigned short CSingleChannelTIFFData::GetCPI()
{
	return m_CPI;
}

int CSingleChannelTIFFData::GetSubsampledWidth()
{
	return (m_Width / SUBSAMPLERATE);
}

int CSingleChannelTIFFData::GetSubsampledHeight()
{
	return (m_Height / SUBSAMPLERATE);
}

bool CSingleChannelTIFFData::GetSubsampleImage(unsigned short *image)
{
	bool ret = true;
	unsigned short *ptr = image;
	int width = GetSubsampledWidth();
	int height = GetSubsampledHeight();

	for (int i = 0; i < height; i++)
	{
		for (int j = 0; j < width; j++, ptr++)
		{
			*ptr = m_Image[i * SUBSAMPLERATE][j * SUBSAMPLERATE];
		}
	}

	return ret;
}

void CSingleChannelTIFFData::CleanUpExclusionArea(int centerX, int centerY, int radiusX, int radiusY)
{
	int radius = radiusY;
	if (radiusX < radius)
		radius = radiusX;
	radius -= 10;
	int i0 = centerY - radius;
	int i1 = centerY + radius;
	for (int i = 0; i < m_Height; i++)
	{
		if ((i <= i0) || (i >= i1))
		{
			for (int j = 0; j < m_Width; j++)
				m_Image[i][j] = 0;
		}
		else 
		{
			int i2 = centerY - i;
			int j2 = radius * radius - i2 * i2;
			j2 = (int)sqrt((double)j2);
			for (int j = 0; j < (centerX - j2); j++)
				m_Image[i][j] = 0;
			for (int j = (centerX + j2); j < m_Width; j++)
				m_Image[i][j] = 0;
		}
 	}
}
